module.exports = {
  stub: {
    key: '3412ED4545454545AD45454545454545',
    nonce: 'af1e865642ab461da572741b11248b2c'
  },
  server: {
    evoProto: {
      host: '0.0.0.0',
      port: 80
    },
    mobile: {
      host: '0.0.0.0',
      port: 8080
    }
  },
  cipher: {
    jwt_secret: 'dat+ass+gegenmann',
    bc_rounds: 4
  },
  transports: {
    mail: {
      host: 'smtp.yandex.ru',
      port: '465',
      secure: true,
      auth: {
        user: 'support@evo.by',
        pass: 'evo.byevo.by'
      },
      from: 'EVO.by robot <support@evo.by>'
    }
  },
  db: {
    customers: 'db/evo.customers.sqlite',
    jwt: 'db/jwt.db',
    devices: 'db/evo.devices.db'
  },
  app: {
    name: 'EVO cloud',
    version: '1.0',
    build: 1
  }
};