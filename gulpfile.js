// install: npm install -D gulp gulp-nodemon
var gulp = require('gulp');
var nodemon = require('gulp-nodemon');

gulp.task('dev', function(){
  nodemon({
    script: 'app.js',
    env: {
      NODE_ENV: 'development',
      EVO_HOST: '0.0.0.0',
      EVO_PORT: 8091,
      MOBILE_HOST: '0.0.0.0',
      MOBILE_PORT: 8092
    }
  })
    .on('restart', function(){
      console.warn('Server restarted');
    });
});

gulp.task('prod', function(){
  nodemon({
    script: 'app.js',
    env: {
      NODE_ENV: 'stage'
    }
  })
    .on('restart', function(){
      console.warn('Stage server restarted');
    });
});

gulp.task('default', ['dev']);