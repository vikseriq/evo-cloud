module.exports = {
  mail: {
    'reset': {
      'subject': 'Восстановление доступа',
      'html': '<p><%- name %>,<br/>Вы запросили сброс пароля для сервиса evo.by.</p><p>Новый пароль: <b><%- password %></b>.</p><small>🤘</small>'
    }
  }
};