const proto = require('./../proto');
var net = require('net');

var writeStatus = function(socket, compact){
  if (!connectionActive)
    return;
  var json = {
    state: compact ? '22' : '10',
    time: new Date().getTime()
  };
  var encoded = Buffer.from(proto.encode(json));
  var data = [
    'POST /puppet HTTP 1.1',
    'Host: api.evo.by',
    'Connection: keep-alive',
    'User-Agent: evo.debug;v.0.0.1',
    'X-Evo-Device: node-debug',
    'Content-Type: application/octet-stream',
    'Content-Length: ' + encoded.length,
    '', ''
  ].join("\n");

  if (!compact){
    socket.write(data);
    socket.write(encoded);
  } else {
    data = [
      'POST /keep HTTP 1.1',
      'Content-length: ' + encoded.length,
      '', ''
    ].join("\n");
    socket.write(Buffer.concat([Buffer.from(data), encoded]));
  }
  console.log('status send', compact ? 'compact' : 'with headers');
};

var chunkBuffer = Buffer.from('chunked');
var connectionActive = false;

var client = new net.Socket();
client.connect(process.env.PORT || 80, process.env.SERVER || 'api.evo.by', function(){
  console.log('Connected');
  connectionActive = true;
  writeStatus(client);
});
client.on('data', function(data){
  console.log('Received: ' + data.length);
  //console.log(Buffer.from(data).toString('hex'));
  if (data.indexOf(Buffer.from('HTTP')) == 0){
    // http chunk with headers
    var stop = data.indexOf(chunkBuffer);
    // end of headers
    stop = data.indexOf(0x0d0a0d0a, stop);
    // end of chunk size mark
    stop = data.indexOf(0x0d0a, stop + 4);
    if (stop == 0 || stop >= data.length)
      return;
    data = data.slice(stop + 1, -2);
    console.log(proto.decode(data));

    setTimeout(function(){
      writeStatus(client, true);
    }, 2000);
  } else {
    // raw data
    console.log(proto.decode(data));
    setTimeout(function(){
      writeStatus(client, true);
    }, 2000);
  }
});
client.on('error', function(error){
  console.log('error', error);
});
client.on('close', function(){
  console.log('Connection closed');
  connectionActive = false;
});