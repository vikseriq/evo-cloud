# EVO cloud

Node.js implementation of evo-home cloud server. For fun and profit.

## Proto

Specification: [NewSite.Confluence](http://conf.jira.by:8090/pages/viewpage.action?pageId=119734288)

AES-128 + CRC -> json

## Setup

```bash
$ npm instal
```

## Server

HTTP 1.1 with keep-alive

```bash
$ PORT=8080 node app.js
```

## Tests

Proto client

```bash
$ PORT=8080 SERVER=localhost npm test
```

## License

MIT &copy; 2017 vikseriq