const readBody = require('raw-body');
const proto = require('./../proto');
const utils = require('./../utils');
const log = require('./../logger');
const states = require('./../states');
const _ = require('lodash');
const mobiles = require('./mobiles');

var Remotes = function(){
  this.sockets = [];
  this._lastSocketId = 0;
};

Remotes.prototype.socketForDevice = function(deviceId){
  if (!deviceId)
    return null;
  return _.find(_.sortBy(this.sockets, 'access').reverse(), {deviceId: deviceId});
};

Remotes.prototype.socketRegister = function(socket){
  var id = ++this._lastSocketId;
  this.sockets.push({
    id: id,
    init: new Date().getTime(),
    access: 0,
    gone: 0,
    deviceId: '',
    state: states.createState(),
    lastState: [],
    remote: {
      address: socket.remoteAddress,
      port: socket.remotePort
    },
    _socket: socket
  });
  log.c('[remotes] registered socket ' + id);
  return id;
};

Remotes.prototype.socketOnData = function(socket, data){
  log.c('[sockets] onData \t' + socket.id + '\t' + data.length);
};

Remotes.prototype.socketOnClose = function(socket){
  var sock = this.getSocket(socket);
  sock.gone = new Date().getTime();
  mobiles.emitServerStatus(sock);
  sock.deviceId += '_OFFLINE';
  log.c('[sockets] onClose ' + socket.id);
  log.l('[sockets] \t\t\t\t closed ' + socket.id);
  // remove server from stack
  var i = _.findIndex(this.sockets, {id: sock.id});
  if (i >= 0)
    this.sockets.splice(i, 1);
};

Remotes.prototype.getSocket = function(x){
  var id = 0;
  if (typeof x == 'number'){
    // by id
    id = x;
  } else if (x.socket){
    // from request
    id = x.socket.id;
  } else {
    // from socket
    id = x.id;
  }
  return _.find(this.sockets, {id: id})
};

Remotes.prototype.sendKeepAlive = function(socket){
  socket.write("LOL :D");
  log.c('[remotes] sent keepalive');
};

Remotes.prototype.sendRaw = function(socket, data){
  var out = proto.encode(data);
  var ok = socket.write(out);
  log.c('[remotes] sent raw state: ' + (ok ? 'ok' : 'fail'));
};

Remotes.prototype.sendHTTP = function(response, data){
  var out = proto.encode(data);

  response.state = 200;
  response.setHeader('Content-Type', 'octet/stream');
  //response.setHeader('Content-Length', out.length);

  var ok = response.write(out);
  log.c('[remotes] sent state: ' + (ok ? 'ok' : 'fail'));
};

const fail = function(_){
  throw _;
};

Remotes.prototype.handle = function(request, response){
  var sock = this.getSocket(request);
  if (!sock)
    throw 'Unrecognized socket';
  sock.access = new Date().getTime();

  try {
    if (request.url == '/keep'){
      // request through open socket
      if (!sock.deviceId)
        throw 'Connection not initialized';

      return readBody(request).then(buf =>{
        var json = proto.decode(buf);
        log.c('[remotes] keeper from peer, state: ' + json.state);
        if (log.level.keeper)
          log.logger.info(json);

        if (json.state != 10){
          this.sendRaw(request.socket, states.receivedReply());
          sock.state.applyHex(json);
          mobiles.emitServerStatus(sock);
        }
      }).catch(fail);
    } else if (request.url == '/puppet'){
      if (!request.headers['x-evo-device'])
        throw 'Unknown device';
      sock.deviceId = request.headers['x-evo-device'];
      log.logger.info('EVO device: %s', request.headers['x-evo-device']);

      var body_length = request.headers['content-length'];
      if (!body_length)
        throw 'Invalid payload length';

      return readBody(request, {length: body_length}).then(buf =>{
        var json = proto.decode(buf);
        log.c('[remotes] initial packet from peer, state: ' + json.state);
        log.logger.info(sock.lastState);
        this.sendHTTP(response, states.receivedReply());
        sock.state.applyHex(json);
        mobiles.emitServerStatus(sock);
      }).catch(fail);
    }
  } catch (_){
    console.log('[remotes] error:', _);
    response.statusCode = 401;
    response.end('401 ' + _);
  }
};

Remotes.prototype.initSocket = function(socket){
  var self = this;
  socket.id = this.socketRegister(socket);
  socket.on('data', function(_){
    self.socketOnData(this, _);
  });
  socket.on('close', function(_){
    self.socketOnClose(this, _);
  });
  socket.setKeepAlive(true);
  socket.setTimeout(60 * 1000);
};

module.exports = new Remotes;