const log = require('../logger').logger;
const remotes = require('./remotes');
const _ = require('lodash');
const readBody = require('raw-body');
const states = require('./../states');
const db = require('./../db');

const API_PATH = '/api';

let Mobiles = function(){
  this.sockets = [];
  this.routes = {
    '/auth/register/': (db.customers.accountCreate).bind(db.customers),
    '/auth/login/': (db.customers.accountAuthenticateProfile).bind(db.customers),
    '/auth/forget/': (db.customers.accountResetPassword).bind(db.customers),
    '/profile/house/': (function(data, req){
      return this.checkRequestToken(req).then(()=>{
        if (req.method == 'POST')
          return this.profile.setHouse(data);
        return this.profile.getHouses();
      });
    }).bind(db.customers)
  };
};

Mobiles.prototype.err = function(res, code){
  code = code || 'nothing';
  var reply = {
    code: 400,
    error: code
  };
  switch (code){
    case 'nothing':
      // default
      reply.code = 418;
      reply.error = 'Nothing here';
      break;
  }
  res.statusCode = reply.code;
  res.end(JSON.stringify(reply));
};

Mobiles.prototype.handle = function(req, res){
  var headers = {};
  headers["Access-Control-Allow-Origin"] = req.headers.origin || '*';

  if (req.method == 'OPTIONS'){
    headers["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS";
    headers["Access-Control-Max-Age"] = '86400';
    headers["Access-Control-Allow-Headers"] = "Authorization, X-Authorization, X-Requested-With, Content-Type, Accept";
    res.writeHead(200, headers);
    res.end();
    return;
  } else {
    _.each(headers, function(v, k){
      res.setHeader(k, v);
    })
  }

  // parse body and handle request
  return readBody(req).then(data=>{
    // lookup for method
    var methodRoute = null;
    _.each(this.routes, function(_, route){
      if (req.url == API_PATH + route){
        methodRoute = route;
        return false;
      }
    });
    // unregistered route
    if (!methodRoute)
      throw 'unknown_method';

    // parse body
    var payload = null;
    if (req.method != 'GET'){
      try {
        payload = JSON.parse(data);
      } catch (e) {
        throw 'wrong_data_format';
      }
    }

    log.verbose('Handling %s', methodRoute);
    // apply method
    return this.routes[methodRoute].call(null, payload, req);
  }).then(_=>{
    log.verbose('Method invocation successful', _);
    res.statusCode = 200;
    // success responce
    res.end(JSON.stringify({
      code: 200,
      data: _
    }));
  }).catch(_=>{
    // responce with error
    log.warn('Can not invoke', _);
    this.err(res, _);
  })
};


Mobiles.prototype.methodSet = (data)=>{
  return new Promise((done, fail)=>{
    var remotes = require('./remotes');
    // find device
    if (!data.data && !data.data.length == 1)
      return fail('bad_payload');

    var set_data = data.data[0];
    if (!set_data['server'])
      return fail('bad_payload');

    var server = remotes.socketForDevice(set_data['server']);
    if (!server)
      return fail('server_offline');

    var state = server.state.applyDiff(set_data);
    log.info('setting value ', set_data, state);
    remotes.sendRaw(server._socket, state);

    //var outState = states.asArrays(server.lastState);
    //outState.server = server.deviceId;
    //done({status: outState, _serverState: state});
    require('./mobiles').emitServerStatus(server);

    done({status: 'success'});
  });
};


Mobiles.prototype.emitServerStatus = _.throttle(function(server){
  if (typeof server == 'string'){
    server = require('./remotes').socketForDevice(server);
  }
  if (!server)
    return;
  var outState = server.state.formatMobile();
  outState.server = server.deviceId;
  outState.online = !server.gone;

  log.verbose('SOCKET.IO notify status change');

  _.each(this.sockets, function(socket){
    socket.emit('statusUpdated', {status: outState});
  })
}, 150, {leading: true, trailing: true});

Mobiles.prototype.initConnection = function(socket){
  var self = this;
  log.info('SOCKET.IO connected');
  socket
    .on('disconnect', function(_){
      log.info('SOCKET.IO gone');
    })
    .on('verify', function(token){
      db.customers.checkToken(token).then(()=>{
        socket.emit('verified', 'U COOL');
        self.sockets.push(socket);
        _.each(_.without(require('./remotes').sockets, {deviceId: ''}), function(server){
          self.emitServerStatus(server);
        });
      }).catch(()=>{
        socket.disconnect();
      })
    })
    .on('statusSet', function(data){
      log.verbose('SOCKET.IO status set', data);
      self.methodSet(data);
    })
    .on('statusRequest', function(_){
      log.verbose('SOCKET.IO received', _);
    });
  //socket.emit('statusUpdated', {status: {server, relay, dimmer}});
};

Mobiles.prototype.methodMap = {
  'set': 'methodSet',
  'auth': 'methodAuth',
  'config': 'methodConfig'
};


module.exports = new Mobiles();