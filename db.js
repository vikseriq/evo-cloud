const sqlite = require('sqlite');
const Customers = require('./models/customers');

var Database = function(config){
  this.customers = new Customers();
  Promise.all([
    sqlite.open(config.customers)
  ]).then(([dbCustomers])=>{
    this.customers.init(dbCustomers);
  })
};

module.exports = new Database(require('./conf').db);
