const winston = require('winston');
const log_level = {
  winston: true,
  clog: !true,
  keeper: false
};

var winston_transports = [];
if (log_level.winston){
  winston_transports.push(new (winston.transports.Console)({
    timestamp: _=>{
      return new Date().toTimeString();
    },
    colorize: true,
    prettyPrint: true
  }));
}

var logger = new (winston.Logger)({
  level: 'verbose',
  transports: winston_transports
});

var last_time = 0;
var clog = function(...data){
  if (!log_level.clog)
    return;

  var time = new Date().getTime();
  if (!last_time)
    last_time = time;
  var args = [(time - last_time) + 'ms\t'];
  for (var i in arguments)
    args.push(arguments[i]);

  console.log.apply(this, args);

  last_time = time;
};

module.exports = {
  c: clog,
  l: console.log,
  logger: logger,
  level: log_level
};