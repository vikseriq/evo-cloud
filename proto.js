var aesjs = require('aes-js');
var utils = require('./utils');
var conf = require('./conf');
var key = new Buffer(conf.stub.key, 'hex');
var nonce = new Buffer(conf.stub.nonce, 'hex');

module.exports = {
  encode: function(json){
    var out_string = Buffer.from(JSON.stringify(json));
    var out_crc = utils.crc32(out_string);
    var out_buffer = Buffer.alloc(16 * Math.ceil((out_string.length + 5) / 16));
    out_buffer.writeUInt32BE(out_crc, 0);
    out_string.copy(out_buffer, 4);

    var aes = new aesjs.ModeOfOperation.cbc(key, nonce);
    var out_data = aes.encrypt(out_buffer);
    return new Buffer(out_data);
  },

  decode: function(packet_data){
    var aes = new aesjs.ModeOfOperation.cbc(key, nonce);
    var decrypt = aes.decrypt(packet_data);
    var packet_crc = new Buffer(decrypt.slice(0, 4)).toString('hex');
    var p_div = 4, p_n = decrypt.length;
    while (decrypt[++p_div] != 0x00 && p_div < p_n);
    var data = decrypt.slice(4, p_div);
    var data_crc = utils.asHex(utils.crc32(data), 8);

    if (data_crc != packet_crc)
      throw 'Wrong crc: expect ' + packet_crc + ' got ' + data_crc;

    var json = {};
    try {
      json = JSON.parse(Buffer.from(data).toString());
    } catch (e){
      throw 'Wrong package';
    }

    return json;
  }
};