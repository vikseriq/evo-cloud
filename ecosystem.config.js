module.exports = {
  apps: [
    {
      name: 'api',
      script: 'app.js',
      env: {
        COMMON_VARIABLE: 'true'
      },
      env_production: {
        NODE_ENV: 'production'
      },
      watch: true,
      ignore_watch: ['db', 'tests', 'logs', 'assets', '\.*']
    }
  ]
};
