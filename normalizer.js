const _ = require('lodash');

const defaultCountryCode = '375';

var formatters = {
  phone: function(data){
    data = data
      .replace(/[^\d+]+/g, '')
      .replace(/^80/, '+' + defaultCountryCode);
    if (data.length < 7 + 3)
      return false;
    if (!data.match(/^\+/))
      data = '+' + data;
    return data;
  },
  email: function(data){
    data = data.toLowerCase().trim();
    if (!/^([\w-]+(?:\.[\w-]+)*)(\+[\w\.-]+)?@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,63}(?:\.[a-z]{2})?)$/i.test(data))
      return false;
    return data;
  },
  password: function(data){
    if (data.length < 6)
      return false;
    return data;
  },
  string: function(data){
    data = data.trim();
    if (!data.length)
      return false;
    return data;
  },
  text: function(data){
    return data;
  }
};

var Normalizer = {
  norm: function(scheme, data, options){
    var fail = [];
    var out = {};
    options = _.assign({
      keepMissing: false,
      valueDefault: ''
    }, options);

    _.each(scheme, function(v, k){
      var value = data[k] || options.valueDefault;
      value = formatters[v](value, options);
      if (value === false)
        fail.push(k);
      out[k] = value;
    });

    if (options.keepMissing)
      out = _.assign(out, data);

    out._nenorm = (fail.length) ? fail : false;
    return out;
  }
};

module.exports = Normalizer;