const utils = require('./utils');
const _ = require('lodash');

var States = {

  receivedReply: function(){
    return {
      state: '10',
      time: utils.ts()
    };
  },

  createState: function(){
    return new State();
  }
};

var State = function(){
  var zeros = '0'.repeat(16).split('').map(function(x){ return +x; });
  this.deviceId = '';
  this.relay = _.clone(zeros);
  this.dimmer = _.clone(zeros);
  this.sensor = _.clone(zeros);
};

State.prototype.applyHex = function(server_state, device_id){
  var b, i;

  if (device_id)
    this.deviceId = device_id;

  if (server_state.relay){
    b = utils.asBit.fromHexString(server_state.relay).toString();
    b = '0'.repeat(16 - b.length) + b;
    this.relay = b.split('').reverse().map(function(x){ return +x; });
  }

  if (server_state.dimmer){
    b = Buffer.from(server_state.dimmer, 'hex');
    for (i = 0; i < b.length; i++){
      this.dimmer[i] = b[i];
    }
  }

  if (server_state.sensor){
    b = Buffer.from(server_state.sensor, 'hex');
    for (i = 0; i < b.length; i++)
      this.sensor[i] = b[i];
  }
};

State.prototype.formatMobile = function(){
  return {
    //deviceId: this.deviceId,
    relay: this.relay,
    dimmer: this.dimmer,
    sensor: this.sensor
  }
};

State.prototype.formatHex = function(){
  return {
    relay: this.relay.join(' '),
    dimmer: this.relay.join(' '),
    sensor: this.relay.join(' ')
  }
};

State.prototype.applyDiff = function(data){
  var json = {
    state: '13',
    time: utils.ts(),
    set: []
  };
  var value;
  var mask = utils.asBit(0);
  mask.set(data.port, 1);

  if (data.type == 'relay'){
    // set self
    this.relay[data.port] = data.value;

    // value as bit array
    value = utils.asBit(0);
    value.set(data.port, data.value);
    json.set.push({
      type: 1,
      mask: utils.asHex(mask, 4),
      value: utils.asHex(value, 4),
      bin: utils.octet(mask, 2, 16)
    })
  } else if (data.type == 'dimmer'){
    // set self
    this.dimmer[data.port] = data.value;

    value = data.value;
    json.set.push({
      type: 2,
      mask: utils.asHex(mask, 4),
      value: utils.asHex(value, 2),
    });
  }

  return json;
};

module.exports = States;