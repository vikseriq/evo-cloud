const _ = require('lodash');
const utils = require('../utils');
const cuid = require('cuid');

let Profile = function(db, customers){
  this.customers = customers;
  this.db = db;
};

Profile.prototype.customers = {};

Profile.prototype.getAccountAccess = function(account){
  return new Promise((done, fail)=>{
    let id = account.id ? account.id : account;
    this.db.all("SELECT * FROM access WHERE account = '" + id + "'")
      .then(levels=>{
        done(levels);
      });
  });
};

Profile.prototype.getHouses = function(){
  return new Promise((done)=>{
    // account access
    this.getAccountAccess(this.customers.token)
      .then((acl)=>{
        // only granted houses
        let houses = _.map(_.filter(acl, function(e){
          return e.level > 0;
        }), 'house');
        return this.db.all("SELECT * FROM houses WHERE house IN ('" + houses.join("', '") + "') ORDER BY created ASC");
      })
      .then(function(houses){
        // prepare data
        return _.map(houses, function(e){
          return {
            id: e.house,
            data: JSON.parse(e.data)
          }
        });
      })
      .then(function(house_data){
        // TODO: get users
        _.each(house_data, function(e){
          e.users = [];
        });
        done(house_data);
      });
  });
};

Profile.prototype.createHouse = function(house_data){
  return new Promise((done, fail)=>{
    house_data.id = cuid();
    let time = utils.ts();
    let user = this.customers.token.id;

    this.db.run("INSERT INTO houses (house, data, created, updated, updated_by) VALUES (?, ?, ?, ?, ?)", [
      house_data.id, JSON.stringify(house_data.data), time, time, user
    ]).then(()=>{
      this.db.run("INSERT INTO access (account, house, level, granted_on, granted_till, granted_by, confirmed_on) " +
        "VALUES(?, ?, ?, ?, ?, ?, ?)", [
        user, house_data.id, 7, time, 0, user, time
      ])
    }).then((_)=>{
      done(house_data.id);
    })
  })
};

/**
 * Unlink house from current account
 *
 * @param house_data
 */
Profile.prototype.removeHouse = function(house_data){
  return new Promise((done, fail)=>{
    let user = this.customers.token.id;
    this.db.run("DELETE FROM access WHERE house = ? AND account = ?",
      house_data.id, user
    ).then(()=>{
      done(house_data.id);
    })
  });
};

Profile.prototype.setHouse = function(house_data){
  const valid_remove = (house_data && typeof house_data.id == 'string' && !!house_data.remove);
  const valid_update = (house_data && typeof house_data.data == 'object' && typeof house_data.id == 'string');
  if (!(valid_update || valid_remove))
    throw 'bad_format';

  if (valid_remove)
    return this.removeHouse(house_data);

  if (house_data.id == 'new')
    return this.createHouse(house_data);

  return new Promise((done, fail)=>{
    this.getAccountAccess(this.customers.token)
      .then((acl)=>{
        // check access for this home
        let level = 0;
        _.each(acl, function(e){
          if (e.house == house_data.id){
            level = e.level;
            return false;
          }
        });
        if (level < 6)
          return fail('access_denied');
        return level;
      })
      .then((level)=>{
        // update
        return this.db.run("UPDATE houses SET updated = ?, updated_by = ?, data = ? WHERE house = ?", [
          utils.ts(),
          this.customers.token.id,
          JSON.stringify(house_data.data),
          house_data.id
        ]);
      })
      .then((_)=>{
        done(house_data.id);
      });
  });
};

module.exports = Profile;