const _ = require('lodash');
const log = require('./../logger').logger;
const norm = require('./../normalizer');
const cuid = require('cuid');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const conf = require('./../conf');
const transports = require('./../external_transport');
const ModelProfile = require('./profile');
const BC_ROUNDS = conf.cipher.bc_rounds;
const utils = require('./../utils');

let Customers = function(){
  this.db = null;
};

/**
 *
 * @type Profile
 */
Customers.prototype.profile = ModelProfile;

/**
 * Token for current session
 *
 * @type {object}
 */
Customers.prototype.token = null;

Customers.prototype.init = function(db){
  this.db = db;
  this.profile = new ModelProfile(db, this);
};

Customers.prototype.checkRequestToken = function(req){
  let authToken = (req.headers.authorization || '').split('Bearer ');
  if (authToken.length > 1){
    return this.checkToken(authToken[1]);
  }
  throw 'unauthorized';
};

Customers.prototype.checkToken = function(token){
  this.token = null;
  return new Promise((done, fail)=>{
    //log.verbose('token', token);
    jwt.verify(token, conf.cipher.jwt_secret, (err, decoded)=>{
      if (err)
        return fail('token_inactive');
      this.token = decoded;
      //log.verbose('decoded token', decoded);
      done(true);
    })
  });
};

Customers.prototype.issueToken = function(account){
  return new Promise((done, fail)=>{
    jwt.sign({
      _: Math.ceil(+new Date() / 1000),
      id: account.id,
      name: account.name,
      email: account.email
    }, conf.cipher.jwt_secret, {
      algorithm: 'HS512',
      expiresIn: '2d',
      issuer: 'evo.mobile'
    }, (err, token)=>{
      if (err){
        log.warn('token issuer', err);
        return fail('token_not_issued');
      }
      return done(token);
    });
  });
};

/**
 * Authenticate and return profile info
 * @param payload
 */
Customers.prototype.accountAuthenticateProfile = function(payload){
  return new Promise((done, fail)=>{
    this.accountAuthenticate(payload).then(account=>{
      this.issueToken(account).then(token=>{
        let info = {
          user: {
            id: account.id,
            name: account.name
          },
          token: token
        };
        done(info);
      });
    }).catch(_=>{
      fail(_);
    })
  });
};

/**
 * Authentication: check creds, return account or fail
 * @param payload
 * @returns {Promise}
 */
Customers.prototype.accountAuthenticate = function(payload){
  return new Promise((done, fail)=>{
    // validate input data
    if (!payload || !payload.login || !payload.password)
      return fail('need_data');
    payload.phone = payload.email = payload.login;
    let data = norm.norm({
      'phone': 'phone',
      'email': 'email',
      'password': 'password'
    }, payload);
    let creds = [];
    if (data.phone)
      creds.push({'phone': data.phone});
    else if (data.email)
      creds.push({'email': data.email});
    if (!data.password || !creds.length)
      return fail('need_data');

    // fetch account
    this.accountLookup(creds)
      .then(account=>{
        // check creds
        bcrypt.compare(data.password, account.haslo)
          .then(isOk=>{
            if (!isOk){
              bcrypt.compare(data.password, account.haslo_temp)
                .then(isOk=>{
                  if (!isOk)
                    return fail('account_bad_credentials');

                  // set temp password as primary
                  this.db.run("UPDATE `accounts` SET haslo = haslo_temp, haslo_temp = '' WHERE id = ?", [account.id]);

                  return done(account);
                })
                .catch(_=>{
                  log.warn('authenticator', _);
                  fail('account_bad_credentials');
                })
            } else {
              // primary password is ok
              done(account);
            }
          })
          .catch(_=>{
            log.warn('authenticator', _);
            fail('account_bad_credentials');
          })
      })
      .catch(()=>{
        fail('account_not_found');
      });
  });
};

/**
 * Find any user account matching terms
 * @param terms
 * @param strategy
 * @returns {Promise}
 */
Customers.prototype.accountLookup = function(terms, strategy){
  strategy = strategy || 'row';
  return new Promise((done, fail)=>{
    var cases = [];
    _.each(terms, function(rules){
      var variants = [];
      _.each(rules, function(value, key){
        variants.push("`" + key + "` = '" + value + "'");
      });
      cases.push(variants.join(' AND '));
    });
    var where = "WHERE (" + cases.join(') OR (') + ")";
    if (strategy == 'count'){
      this.db.get("SELECT COUNT(*) as k FROM `accounts` " + where)
        .then(result=>{
          done(result.k);
        });
    } else if (strategy == 'row'){
      log.info(where);
      this.db.get("SELECT * FROM `accounts` " + where)
        .then(result=>{
          done(result);
        })
    } else {
      fail('unknown_strategy');
    }
  });
};

Customers.prototype.accountResetPassword = function(dataIn){
  return new Promise((done, fail)=>{
    const data = norm.norm({
      'login': 'email'
    }, dataIn);
    if (data._nenorm)
      return fail('reset_email_only');

    this.accountLookup([{'email': dataIn.login}])
      .then((row)=>{
        const info = {
          password: utils.password(),
          name: row.name
        };
        this.db.run("UPDATE `accounts` SET haslo_temp = ? WHERE id = ?", [bcrypt.hashSync(info.password, BC_ROUNDS), row.id]);
        transports.mail.send(row.email, 'reset', info);
        done(row.email);
      })
  });
};

/**
 * Create user account
 * @param dataIn
 * @param options
 * @returns {Promise}
 */
Customers.prototype.accountCreate = function(dataIn, options){
  options = options || {};
  log.verbose('Creating account with data', dataIn);
  return new Promise((done, fail)=>{
    // validate input
    const data = norm.norm({
      'phone': 'phone',
      'email': 'email',
      'password': 'password',
      'name': 'text'
    }, dataIn);
    log.verbose('validation result', data);
    if (data._nenorm)
      return fail('need_data');

    // check for duplicate
    this.accountLookup([{'phone': data.phone}, {'email': data.email}], 'count')
      .then(n=>{
        if (n > 0)
          return fail('account_already_exists');
        const id = cuid();
        this.db.run("INSERT INTO `accounts` (`id`, `email`, `phone`, `haslo`, `name`, `created`, `updated`)" +
          " VALUES (?, ?, ?, ?, ?, ?, 0)", [
            id, data.email, data.phone, bcrypt.hashSync(data.password, BC_ROUNDS),
            data.name, +new Date()
          ]
        ).then(_=>{
          log.verbose('User created', _);
          log.info('User created', id);
          done(options.explode ? id : true);
        })
      })
      .catch(_=>{
        log.error('db error:', _);
        // proxies inner failures
        if (typeof _ == 'string')
          fail(_);
        else
          fail('db_error');
      });
  });
};

module.exports = Customers;