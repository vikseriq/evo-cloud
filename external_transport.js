const _ = require('lodash');
const conf = require('./conf');
const nodemailer = require('nodemailer');

const mail_templates = require('./messages').mail;

const mailer = {
  transport: nodemailer.createTransport(conf.transports.mail),

  send: function(to, type, options){
    const template = mail_templates[type];

    var mail = {
      from: conf.transports.mail.from,
      to: to,
      bcc: 'w@newsite.by',
      subject: template.subject,
      html: _.template(template.html)(options)
    };
    this._send(mail);
  },

  _send: function(mail){
    this.transport.sendMail(mail, (err, data)=>{
      if (err)
        return console.error(err);

      console.log('mail sent', data.messageId);
    })
  }
};

module.exports = {
  mail: mailer
};