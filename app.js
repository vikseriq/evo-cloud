const db = require('./db');
const http = require('http');
const conf = require('./conf');
const utils = require('./utils');
const socketio = require('socket.io');
const logger = require('./logger');
const log = logger.logger;
const assets = require('./handlers/assets');
const remotes = require('./handlers/remotes');
const mobiles = require('./handlers/mobiles');
const evo_host = process.env.EVO_HOST || conf.server.evoProto.host;
const evo_port = process.env.EVO_PORT || conf.server.evoProto.port;
const mobile_host = process.env.MOBILE_HOST || conf.server.mobile.host;
const mobile_port = process.env.MOBILE_PORT || conf.server.mobile.port;
const appName = utils.appName(conf.app, process.env.DEV);
const _ = require('lodash');

process.on('unhandledRejection', error => {
  log.error('unhandledRejection', error);
});

const server = http.createServer(function(request, response){
  response.setHeader('Server', appName);
  response.setHeader('Connection', 'keep-alive');

  if (request.url != '/keep' || logger.level.keeper){
    const addr = request.socket.address();
    log.info('%s\t\t%s\t%s', addr.address, request.method, request.url);
  }

  if (request.method == 'POST' && (request.url == '/keep' || request.url == '/puppet')){
    // handle slave EVO servers
    return remotes.handle(request, response);
  } else if (request.url.startsWith('/status')){
    response.end(JSON.stringify({servers: _.map(_.reject(remotes.sockets, {deviceId: ''}), function(server){
      return {deviceId: server.deviceId, connection: server.remote, data: server.state.formatHex()}
    })}, null, 4));
  } else if (request.method == 'GET'){
    return assets.handle(request, response);
  }

  response.statusCode = 400;
  response.end('400 Bad request');
});

server
  .on('connection', socket =>{
    remotes.initSocket(socket);
  })
  .on('listening', _ =>{
    log.info('evo.proto server started on %s:%s', evo_host, evo_port);
  })
  .on('error', e =>{
    log.error('Can`t start server: ', e.message);
  })
  .listen(evo_port, evo_host);


const mobileServer = http.createServer(function(request, response){
  if (request.url.startsWith('/api/')){
    // handle mobile API requests
    return mobiles.handle(request, response);
  }
  response.statusCode = 404;
  response.end('404 Not found');
});
mobileServer.on('listening', _ => {
  log.info('evo.rest server started on %s:%s', mobile_host, mobile_port);
});
mobileServer.listen(mobile_port, mobile_host);
socketio(mobileServer)
  .on('connection', function(socket){
    mobiles.initConnection(socket);
  });